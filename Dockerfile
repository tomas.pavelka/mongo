version: '3.7'
services:
  mongodb_container:
  WORKDIR  /usr/src/app
    image: mongo:latest
    environment:
      MONGO_INITDB_ROOT_USERNAME: root
      MONGO_INITDB_ROOT_PASSWORD: OloopoIsTheBest2023
    ports:
      - 27017:27017
    volumes:
      - mongodb_data_container:/data/mongodb
    container_name: oo-mongo
    restart: always
    logging:
      options:
        max-size: 1g
volumes:
  mongodb_data_container: